[v10.3.4]
*Core

CHANGE: (!1151) Python 2.7 is no longer supported

*TransformationSystem

FIX: (!1150) Issues with ByRun plugin found when running stripping validation

*ProductionManagement

FIX: (!1149) Calling ProductionManagement.execProductionScript with Python 3
FIX: (!1147) Commit transaction before vacuuming in NotifyAgent
FIX: (!1147) Finding path to cache.db in Python 3 based installations



v10r3p3
Based on DIRAC v7r3p11
LHCbWebDIRAC v5r4p2
LHCbDIRACOS v1r23
*FrameworkSystem

CHANGE: (!1140) take proxy expiry notification email sender address from the service configuration

*Bookkeeping

FIX: (!1139) only override the DQflag if something sensible is found

*dirac-loop

CHANGE: (!1138) add a `--Last` and `--ShowLast` option to reuse the previous set of items or show them
FIX: (!1138) decode the subprocess output

*storage-summary

FIX: (!1138) set the default top directory as it seems it doesn't work if not specified now

*TransformationDebug

FIX: (!1138) avoid exception when sorting taskIDs and one is `None`

*StorageUsageAgent

FIX: (!1138) fix exception due to dictionary iteration while removing items

*Core

CHANGE: (!1137) Revert hack for removing whitespace from options filenames



v10r3p2
Based on DIRAC v7r3p10
LHCbWebDIRAC v5r4
LHCbDIRACOS v1r23
FIX: (!1132) Raise an LHCbApplicationError if LbProdrun exits with an bad returncode
FIX: (!1132) Add workaround for options paths with whitespace (https://lblogbook.cern.ch/Operations/34916)

*Bookkeeping

FIX: (!1130) Remove "More than 1 run/TCK" warnings as it is normal when running Analysis/WG productions
FIX: (!1130) Partially revert !1115 as it breaks the bookkeeping browser
FIX: (!1128) cast runnumbers to string in processJob before logging

*WorkloadManagement

FIX: (!1126) Remove trailing whitespace and show lb-run command in std.out



v10r3p1
Based on DIRAC v7r3p9
LHCbWebDIRAC v5r4
LHCbDIRACOS v1r23
v10r3p0
Based on DIRAC v7r3p9
LHCbWebDIRAC v5r4
LHCbDIRACOS v1r23


v10r3-pre6
Based on DIRAC v7r3p8
LHCbWebDIRAC v5r4-pre3
LHCbDIRACOS v1r23
v10r3-pre5
Based on DIRAC v7r3p5
LHCbWebDIRAC v5r4-pre2
LHCbDIRACOS v1r23
v10r3-pre4
Based on DIRAC v7r3p4
LHCbWebDIRAC v5r4-pre1
LHCbDIRACOS v1r23
*DataManagementSystem

CHANGE: (!1090) LHCbFTS3Plugin remove hack not to transfer between CTA and Echo
CHANGE: (!1090) use root to transfer between EOS and CTA

*TransformationSystem

CHANGE: (!1083) import WorkflowTasks from the new location



v10r3-pre3
Based on DIRAC v7r3p1
LHCbWebDIRAC v5r4-pre1
LHCbDIRACOS v1r23
Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines
Please follow the template:

*DataManagementSystem

CHANGE: (!1085) Add option --CheckMCReplication to `dirac-dms-add-transformation` to check all existing MC replication transformations against active MC paths. Lists all transformations that do not match and hence could be set Completed
FIX: (!1085) format error in `StorageHistoryAgent`
For examples look into release.notes

*Docs

CHANGE: (!1082) CI does not publish coverage results anymore

*Core

NEW: (!1080) Support Python 3 server installation



v10r3-pre2
Based on DIRAC v7r3-pre19
LHCbWebDIRAC v5r4-pre1
LHCbDIRACOS v1r21

v10r3-pre1
Based on DIRAC v7r3-pre18
LHCbWebDIRAC v5r4-pre1
LHCbDIRACOS v1r21
*CI

FIX: (!1063) update apt cache before installing curl

*Production Management

FIX: (!1057) Only flush transformations if there are unused files
