[Docs]


# name of the module to documented, DIRAC, voDIRAC, ExtensionDIRAC
module_name = LHCbDIRAC

# path to the source folder relative to docs.conf, all paths are relative to this file
source_folder = ../src/LHCbDIRAC



[Code]

# building code reference
# where to please the code reference
docs_target_path = ./source/CodeDocumentation
# where Custom docstrings can be found, see
# DIRAC/docs/diracdoctools/CustomizedDocs for an example
customdocs_folder = ./diracdoctools/CustomizedDocs

# add :private-members: to autodoc for matching module
document_private_members = FCConditionsParser
# add :no-inherited: to autodoc for matching module
no_inherited_members =

# only creating dummy files, because they cannot be safely imported due to sideEffects
create_dummy_files = lfc_dfc_copy, lfc_dfc_db_copy, JobWrapperTemplate, PlotCache,
                     PlottingHandler

# do not include these files in the documentation tree
ignore_folders = diracdoctools, /test, /scripts

# Resources_rc is an idiotic file
ignore_files = setup.py,Resources_rc,extra_func.py

[CFG]

# concatenating ConfigTemplates
# which file to use as a base
base_file = ../lhcbdirac.cfg

# where to place the resulting file
target_file = source/AdministratorGuide/Configuration/ExampleConfig.rst

[Commands]
ignore_commands=
# does not have --help, deploys scripts
  dirac-deploy-scripts,
# does not have --help, starts compiling externals
  dirac-compile-externals,
# does not have --help
  dirac-install-client,
# does not have --help
  dirac-framework-self-ping,
# obsolete
  dirac-dms-add-files,
# just prints version, no help
  dirac-version,
# just prints platform, no help
  dirac-platform,
# no doc, purely internal use
  dirac-agent,
# no doc, purely internal use
  dirac-executor,
# no doc, purely internal use
  dirac-service,
# no doc
  lhcb-proxy-init,
# no doc, pure internal use
  getROOTFileGUID,
# no doc
  add-user-DFC

# scripts where the module docstring should be added to the documentation
add_module_docstring = dirac-install



# arbitrary number of these sections can be added, they result in folders with
# rst files for each command matching the pattern. all options are mandatory,
# except indexFile and prefix
[commands.admin]
pattern = admin, accounting, FrameworkSystem, framework, install, utils, dirac-repo-monitor, dirac-jobexec, dirac-info, ConfigurationSystem, Core, rss, transformation, stager, populate
title = Admin
manual =
exclude =
sectionPath = ./source/AdministratorGuide/CommandReference/%(title)s
# indexFile = ./source/AdministratorGuide/CommandReference/index.rst
existingIndex = False

[commands.bkk]
pattern = bookkeeping
title = Bookkeeping
manual =
exclude =
sectionPath = ./source/UserGuide/CommandReference/%(title)s
# indexFile = ./source/UserGuide/CommandReference/index.rst

[commands.dms]
pattern = dms
title = Data Management
manual =
exclude = dirac-dms-add-transformation
sectionPath = source/UserGuide/CommandReference/%(title)s
# indexFile = source/UserGuide/CommandReference/index.rst

[commands.wms]
pattern = wms
title = Workload Management
manual =
exclude =
sectionPath = source/UserGuide/CommandReference/%(title)s
# indexFile = source/UserGuide/CommandReference/index.rst
